'use strict';

(function(document) {

    var addButton = document.querySelector('#addBtn')
    var addForm = document.querySelector('#addForm')
    var addModale = document.querySelector('#addModale')
    var cancelAddBtn = document.querySelector('#cancelAddBtn')

    function addArticle(article) {
        var articleElt = document.createElement('article')
        articleElt.classList.add('article')        

        var cbxElt = document.createElement('div')
        cbxElt.classList.add('col', 'col--cbx')
        cbxElt.innerHTML = '<input type="checkbox" checked="checked" />'
        articleElt.appendChild(cbxElt)      

        var codeElt = document.createElement('div')
        codeElt.classList.add('col')
        codeElt.innerText = article.code
        articleElt.appendChild(codeElt)      

        var titleElt = document.createElement('div')
        titleElt.classList.add('col', 'col--main')
        titleElt.innerHTML = '<h2 class="title-2">' + article.title + '</h2>'
        articleElt.appendChild(titleElt)      

        var authorElt = document.createElement('div')
        authorElt.classList.add('col')
        authorElt.innerHTML = '<h3 class="title-3">' + article.author + '</h3>'
        articleElt.appendChild(authorElt)    

        var createdAtElt = document.createElement('div')
        createdAtElt.classList.add('col')
        var createdAtDate = new Date(article.createdAt)
        createdAtElt.innerText = createdAtDate.toLocaleDateString("fr-FR")
        articleElt.appendChild(createdAtElt)    

        var priceElt = document.createElement('div')
        priceElt.classList.add('col')
        var price = Number(article.price).toFixed(2)
        priceElt.innerText = price + ' €'
        articleElt.appendChild(priceElt) 

        var liElt = document.createElement('li')
        liElt.classList.add('list__content__item')
        liElt.appendChild(articleElt)
        
        document.querySelector('#articleList .list__content').appendChild(liElt)
    }

    function closeModale() {
        addModale.close()
    }

    function openModale() {
        addModale.showModal()
    }

    function init() {

        /* var xhr = new XMLHttpRequest();
        xhr.open('get', 'http://my-json-server.typicode.com/mathieuheliot/courses-api/books', true);
        xhr.responseType = 'json';
        xhr.onload = function() {
            var status = xhr.status;
            if (status == 200) {
                xhr.response.forEach(book => addArticle(book));
            } else {
                console.error(status);
            }
        };
        xhr.send(); */

        fetch('http://my-json-server.typicode.com/mathieuheliot/courses-api/books')
            .then( response => response.json() )
            .then( books => books.forEach(book => addArticle(book)) )
            .catch( error => console.error(error) )
    }
    
    addButton.addEventListener('click', () => {
        openModale()
    })

    addForm.addEventListener('submit', event => {
        event.preventDefault()
        closeModale()
        var data = new FormData(event.target)
        var article = {
            createdAt: new Date().toISOString()
        };
        data.forEach(function(value, key){
            article[key] = value;
        });
        addArticle(article)
        event.target.reset()
    })

    cancelAddBtn.addEventListener('click', () => {
        closeModale()
    })

    init()

}(document))